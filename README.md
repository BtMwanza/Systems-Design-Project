# Systems-Design-Project
**This project was a requirement for my Systems Design and Implementation assignment.**

**The scenario states:**
*"The shop is called Charlotte's Web and specialises in spider-themed jewellery that is sold from a retail outlet in the city's high street. The shop has a high degree of turnover and is looking to commission someone to develop a web-based store that customers can use. It should not be possible for people to buy jewellery directly from the web - all our jewellery is custom made and all that customers should be able to do is book an appointment with one of our specialised spider jewellers. This is an exciting opportunity and we hope that you are as enthused about this task as we are!"*

## What this app does:
This app helps the user create an appointment with the jewellery store on both Android and iPhone devices. The app is developed using [Expo](https://expo.io/) and [React Native](https://reactnative.dev/). The app's functions include creating and deleting appointments. This app requires internet connectivity to perform its intended function. 
 
More features are being added gradually.

#### Updates - 20/06/2020
* Register screen has an upload spot for user avatars (can not be changed after - yet).
* Profile screen shows avatar, user name (both after signing out and logging-in for new users), and email address.
* Home screen has Refresh Control (pull down to refresh) implemented(only on the list).

#### Update - 16/07/2020
* Changes to splashscreen and icon. Logo created by [DesignEvo](https://www.designevo.com/).

#### Update - 02/08/2020
* Fixed delete function on the home screen.
