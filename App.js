import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Ionicons } from '@expo/vector-icons';
import LoadingScreen from './src/screens/auth/LoadingScreen';
import LoginScreen from './src/screens/auth/LoginScreen';
import RegisterScreen from './src/screens/auth/RegisterScreen';
import firebase from './src/api/config';
import HomeScreen from './src/screens/app/HomeScreen';
import BookingScreen from './src/screens/app/PostScreen';
import ProfileScreen from './src/screens/app/ProfileScreen';

// App screens
const AppContainer = createStackNavigator({
  default: createBottomTabNavigator({
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        tabBarIcon: ({
          tintColor
        }) => (<
          Ionicons name='ios-home'
          size={
            24
          }
          color={
            tintColor
          }
        />
          )
      }
    },
    Booking: {
      screen: BookingScreen,
      navigationOptions: {
        tabBarIcon: ({
          tintColor
        }) => (<
          Ionicons name='ios-add-circle'
          size={
            24
          }
          color='#E9446a'
          style={
            {
              shadowColor: '#E9446a',
              shadowOffset: {
                width: 0,
                height: 10
              },
              shadowRadius: 10,
              shadowOpacity: 0.3
            }
          }
        />
          )
      }
    },
    Profile: {
      screen: ProfileScreen,
      navigationOptions: {
        tabBarIcon: ({
          tintColor
        }) => (<
          Ionicons name='ios-person'
          size={
            24
          }
          color={
            tintColor
          }
        />
          )
      }
    }
  }, {
    defaultNavigationOptions: {
      tabBarOnPress: ({
        navigation,
        defaultHandler
      }) => {
        if (navigation.state.key === 'Booking') {
          navigation.navigate('postModal');
        } else {
          defaultHandler();
        }
      }
    },
    tabBarOptions: {
      activeTintColor: '#161f3d',
      inactiveTintColor: '#b8bbc4',
      showLabel: false
    },
    // Show ProfileScreen first
    initialRouteName: 'Profile'
  }),
  postModal: {
    screen: BookingScreen
  }
}, {
  mode: 'modal',
  headerMode: 'none'
});

// Auth screens
const AuthStack = createStackNavigator({
  Login: LoginScreen,
  Register: RegisterScreen
});

// App navigation
export default createAppContainer(
  createSwitchNavigator({
    Loading: LoadingScreen,
    App: AppContainer,
    Auth: AuthStack
  }, {
    // Show LoadingScreen when app is initiated
    initialRouteName: 'Loading'
  })
);
