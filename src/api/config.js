import firebase from 'firebase';


const FirebaseKeys = {
    /* Firebase API keys */
    apiKey: "AIzaSyAJMZO95HhCviwBOuO0H49o8vaDswqtGA4",
    authDomain: "charlottesweb-c2ce4.firebaseapp.com",
    databaseURL: "https://charlottesweb-c2ce4.firebaseio.com",
    projectId: "charlottesweb-c2ce4",
    storageBucket: "charlottesweb-c2ce4.appspot.com",
    messagingSenderId: "832238424933",
    appId: "1:832238424933:web:1d06357f79fa30ad87c78c",
    measurementId: "G-49FFB8S4B2"
};

// initialize Firebase
firebase.initializeApp(FirebaseKeys);


export default FirebaseKeys;
