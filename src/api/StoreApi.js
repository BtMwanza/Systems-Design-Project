import FirebaseKeys from './config';
import firebase from 'firebase';
require("firebase/firestore");

class Fire {
    constructor() {
        FirebaseKeys
    }

    // Check if user is signed in or not
    subscribeToAuthChanges(authStateChanged) {
        firebase.auth().onAuthStateChanged((user) => {
            authStateChanged(user);
        })
    }

    // Sign out function
    signOut(onSignedOut) {
        firebase.auth().signOut()
            .then(() => {
                onSignedOut();
            })
    }

}

Fire.shared = new Fire();
export default Fire;
