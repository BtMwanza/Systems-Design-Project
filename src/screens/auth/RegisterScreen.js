import React from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
  KeyboardAvoidingView,
  Image
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Ionicons } from '@expo/vector-icons';
import UserPermissions from "./../../utilities/UserPermissions";
import * as ImagePicker from "expo-image-picker";
import Fire from './../../api/StoreApi';
import firebase from 'firebase';


export default class RegisterScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  // State variables
  state = {
    user: {
      name: '',
      email: '',
      password: '',
      avatar: null,
    },
    errorMessage: null
  };

  // Create user and update profile
  signUp({ email, password, displayName }) {
    firebase.auth().createUserWithEmailAndPassword(email.trim(), password)
      .then((userInfo) => {
        console.log(userInfo)
        userInfo.user.updateProfile({
          displayName: this.state.user.name,
          photoURL: this.state.user.avatar
        })
          .then(() => { })
      })
  }

  // Pick user avatar
  handlePickAvatar = async () => {
    UserPermissions.getCameraPermission();

    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3]
    });

    if (!result.cancelled) {
      this.setState({ user: { ...this.state.user, avatar: result.uri } });
    }
  };

  componentDidMount() {
    Fire.shared.subscribeToAuthChanges(this.onAuthStateChanged)
  }

  // Chack authentication state
  onAuthStateChanged = (user) => {
    if (user !== null) {
      this.props.navigation.navigate('App');
    }
  }

  // Trigger user creation
  handleSignUp = () => {
    this.signUp(this.state.user);
  };

  render() {
    return (
      <KeyboardAvoidingView style={styles.container}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <ScrollView>
            <View >
              // Screen's Linear gradient
              <LinearGradient
                colors={['rgba(112, 123, 124,0.8)', 'rgba(23, 32, 42, 1)']}
                style={{
                  position: 'absolute',
                  left: 0,
                  right: 0,
                  top: 0,
                  height: 700,
                }}
              />
              <StatusBar barStyle='light-content'></StatusBar>
              <TouchableOpacity
                style={styles.back}
                onPress={() => this.props.navigation.goBack('')}
              >
                <Ionicons
                  name='ios-arrow-round-back'
                  size={32}
                  color='#FFF'
                ></Ionicons>
              </TouchableOpacity>

              // Greeting message
              <View
                style={{
                  position: 'absolute',
                  top: 64,
                  alignItems: 'center',
                  width: '100%'
                }}
              >
                <Text style={styles.greeting}>
                  {'Hi!\nSign up to get started.'}
                </Text>

                // Avatar slot
                <TouchableOpacity style={styles.avatarPlaceholder} onPress={this.handlePickAvatar}>
                  <Image source={{ uri: this.state.user.avatar }} style={styles.avatar} />
                  <Ionicons
                    name="ios-add"
                    size={40}
                    color="#FFF"
                    style={{ marginTop: 1, marginLeft: 2 }}
                  ></Ionicons>
                </TouchableOpacity>

              </View>

              <View style={styles.errorMessage}>
                {this.state.errorMessage && (
                  <Text style={styles.error}>{this.state.errorMessage}</Text>
                )}
              </View>

              // User form
              <View style={styles.form}>
                // Name
                <View>
                  <Text style={styles.inputTitle}>Full Name</Text>
                  <TextInput
                    style={styles.input}
                    onChangeText={name =>
                      this.setState({ user: { ...this.state.user, name } })
                    }
                    value={this.state.user.name}
                  ></TextInput>
                </View>

                // Email
                <View style={{ marginTop: 32 }}>
                  <Text style={styles.inputTitle}>Email Address</Text>
                  <TextInput
                    style={styles.input}
                    autoCapitalize='none'
                    onChangeText={email =>
                      this.setState({ user: { ...this.state.user, email } })
                    }
                    value={this.state.user.email}
                  ></TextInput>
                </View>

                // Password
                <View style={{ marginTop: 32 }}>
                  <Text style={styles.inputTitle}>Password</Text>
                  <TextInput
                    style={styles.input}
                    secureTextEntry
                    autoCapitalize='none'
                    onChangeText={password =>
                      this.setState({ user: { ...this.state.user, password } })
                    }
                    value={this.state.user.password}
                  ></TextInput>
                </View>
              </View>

              // Sign up button
              <TouchableOpacity style={styles.button} onPress={this.handleSignUp}>
                <Text style={{ color: '#fff', fontWeight: '500' }}>Sign up</Text>
              </TouchableOpacity>

              // Navigate to login
              <TouchableOpacity
                style={{ alignSelf: 'center', marginTop: 32 }}
                onPress={() => this.props.navigation.navigate('Login')}
              >
                <Text style={{ color: '#e6ebf0', fontSize: 13 }}>
                  Already have an account?  {' '}
                  <Text style={{ fontWeight: '500', color: '#E9446A' }}>Login</Text>
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#17202a',
  },
  greeting: {
    marginTop: 1,
    fontSize: 15,
    fontWeight: '500',
    textAlign: 'center',
    color: '#FFF'
  },
  form: {
    marginTop: 170,
    marginBottom: 48,
    marginHorizontal: 30
  },
  inputTitle: {
    color: '#e6ebf0',
    fontSize: 15,
    textTransform: 'uppercase'
  },
  input: {
    borderBottomColor: '#8a8f9e',
    borderBottomWidth: StyleSheet.hairlineWidth,
    height: 30,
    fontSize: 20,
    color: '#e6ebf0',
  },
  errorMessage: {
    height: 72,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 30
  },
  button: {
    marginHorizontal: 30,
    backgroundColor: '#0e6251',
    borderRadius: 4,
    height: 52,
    alignItems: 'center',
    justifyContent: 'center'
  },
  error: {
    color: '#e9446a',
    fontSize: 13,
    fontWeight: '600',
    textAlign: 'center'
  },
  back: {
    position: 'absolute',
    top: 20,
    left: 20,
    width: 32,
    height: 32,
    borderRadius: 16,
    backgroundColor: 'rgba(21, 22, 48, 0.1)',
    alignItems: 'center',
    justifyContent: 'center'
  },
  avatarPlaceholder: {
    width: 100,
    height: 100,
    backgroundColor: '#e1e2e6',
    borderRadius: 50,
    marginTop: 18,
    justifyContent: 'center',
    alignItems: 'center'
  },
  avatar: {
    position: 'absolute',
    width: 100,
    height: 100,
    borderRadius: 50
  }
});
