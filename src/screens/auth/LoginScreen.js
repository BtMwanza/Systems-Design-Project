import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  StatusBar,
  LayoutAnimation,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView
} from 'react-native';
import firebase from 'firebase';
import {
  LinearGradient
} from 'expo-linear-gradient';


export default class LoginScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  // State variables
  state = {
    email: '',
    password: '',
    errorMessage: null,
  };

  // Trigger login function
  handleLogin = () => {
    const {
      email,
      password
    } = this.state;
    firebase.auth().signInWithEmailAndPassword(email.trim(), password)
      .catch(error => this.setState({
        errorMessage: error.message
      }));
  };


  render() {
    // Animation
    LayoutAnimation.easeInEaseOut();

    return (
      <KeyboardAvoidingView style={styles.container}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <ScrollView>
            <View>
              // Screen's Linear gradient
              <LinearGradient colors={['rgba(112, 123, 124,0.8)', 'rgba(23, 32, 42, 1)']}
                style={
                  {
                    position: 'absolute',
                    left: 0,
                    right: 0,
                    top: 0,
                    height: 600,
                  }
                }
              />
              <StatusBar barStyle='light-content'></StatusBar>

              // Greeting message
              <Text style={styles.greeting} > {
                'Welcome back.'
              } </Text>

              <View style={styles.errorMessage} > {
                this.state.errorMessage && (
                  <Text style={styles.error} > {
                    this.state.errorMessage
                  } </Text>
                )
              }
              </View>

              // User form
              <View style={styles.form}>
                // Email
                <View>
                  <Text style={styles.inputTitle}> Email Address </Text>
                  <TextInput style={styles.input}
                    autoCapitalize='none'
                    onChangeText={email => this.setState({ email })}
                    value={this.state.email}>
                  </TextInput>
                </View>

                // Password
                <View style={{ marginTop: 32 }}>
                  <Text style={styles.inputTitle}> Password </Text>
                  <TextInput style={styles.input}
                    secureTextEntry autoCapitalize='none'
                    onChangeText={password => this.setState({ password })}
                    value={this.state.password}>
                  </TextInput>
                </View>
              </View>

              // Login button
              <TouchableOpacity style={styles.button}
                onPress={this.handleLogin}>
                <Text style={{ color: '#fff', fontWeight: '500' }}> Sign in </Text>
              </TouchableOpacity>

              // Navigate to register
              <TouchableOpacity style={{ alignSelf: 'center', marginTop: 32 }}
                onPress={() => { this.props.navigation.navigate('Register'); }}>
                <Text style={{ color: '#e6ebf0', fontSize: 13 }}>
                  Create new account {' '}
                  <Text style={{ fontWeight: '500', color: '#E9446A' }}> Sign Up </Text>
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#17202a',
  },
  greeting: {
    marginTop: 90,
    fontSize: 18,
    fontWeight: '500',
    textAlign: 'center',
    color: '#e6ebf0',
  },
  form: {
    marginTop: 100,
    marginBottom: 48,
    marginHorizontal: 30
  },
  inputTitle: {
    color: '#e6ebf0',
    fontSize: 15,
    textTransform: 'uppercase'
  },
  input: {
    borderBottomColor: '#8a8f9e',
    borderBottomWidth: StyleSheet.hairlineWidth,
    height: 30,
    fontSize: 20,
    color: '#e6ebf0',
  },
  button: {
    marginHorizontal: 30,
    backgroundColor: '#0e6251',
    borderRadius: 4,
    height: 52,
    alignItems: 'center',
    justifyContent: 'center'
  },
  errorMessage: {
    height: 72,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 30
  },
  error: {
    color: '#e9446a',
    fontSize: 13,
    fontWeight: '600',
    textAlign: 'center'
  }
});
