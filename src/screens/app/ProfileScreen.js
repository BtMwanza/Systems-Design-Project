import React from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
import { Button } from 'react-native-elements';
import Fire from './../../api/StoreApi';
import firebase from 'firebase';


export default class ProfileScreen extends React.Component {

  static navigationOptions = (navigation) => ({
    header: null
  });

  // State variables
  state = {
    name: '',
    email: '',
    uid: '',
    avatar: null,
  }

  unsubscribe = null;

  // Get user profile from firebase authentication
  getUserProfile = () => {
    const user = firebase.auth().currentUser;

    if (user != null) {
      // If user is available, change state variables
      this.setState({
        avatar: user.photoURL,
        name: user.displayName,
        email: user.email,
        uid: user.getIdToken(),
      })
    }
  }

  // Sign user out, when button is pressed
  onSignedOut = () => {
    console.log('Signed Out');
    this.props.navigation.navigate('Auth');
  }

  componentDidMount() {
    this.getUserProfile();
  }

  render() {
    return (
      <View style={styles.container}>

        <View style={styles.avatarContainer}>
          <Image
            source={
              { uri: this.state.avatar }
            }
            style={styles.avatar}
          />
        </View>
        <View style={styles.statsContainer}>
          <View style={styles.stat}>
            <Text style={styles.statTitle}>Name</Text>
            <Text style={styles.statValue}>{this.state.name}</Text>
          </View>
          <View style={styles.stat}>
            <Text style={styles.statTitle}>Email</Text>
            <Text style={styles.statValue}>{this.state.email}</Text>
          </View>
        </View>

        <View style={styles.logout}>
          <Button
            onPress={() => { Fire.shared.signOut(this.onSignedOut); }}
            title='Log Out'
            type="outline"
            raised={true}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  logout: {
    marginTop: 1,
    paddingLeft: 80,
    paddingRight: 80,
    paddingTop: 30,
    paddingBottom: 20,
    borderRadius: 100,
    color: 'crimson',
  },
  btn: {
    backgroundColor: 'crimson',
    fontSize: 20,
  },
  statsContainer: {
    flexDirection: "column",
    justifyContent: "space-between",
    margin: 12
  },
  stat: {
    alignItems: "center",
  },
  statValue: {
    color: "#4F566D",
    fontSize: 18,
    fontWeight: "300"
  },
  statTitle: {
    color: "#C3C5CD",
    fontSize: 15,
    fontWeight: "500",
    marginTop: 10,
    paddingTop: 8
  },
  avatarContainer: {
    shadowColor: "#151734",
    shadowRadius: 30,
    shadowOpacity: 0.4,
    alignItems: 'center',
    marginTop: 50,
  },
  avatar: {
    width: 136,
    height: 136,
    borderRadius: 68
  },
});
