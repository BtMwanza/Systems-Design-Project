import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Booking from './DatePicker';

export default class BookingScreen extends React.Component {

  render() {
    return (
      <View style={styles.container}>

        <View style={styles.header}>
          // Go to home screen
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
            <Ionicons name='md-arrow-back' size={24} color='#C70039'></Ionicons>
          </TouchableOpacity>
        </View>

        <View style={styles.container}>
          // Show booking
          <Booking />
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff'
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 32,
    paddingVertical: 12,
    borderBottomWidth: 1,
    borderBottomColor: '#D8D9DB',
    marginTop: 0,
    paddingTop: 40,
  },
});
