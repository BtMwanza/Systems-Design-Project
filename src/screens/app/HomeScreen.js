import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableWithoutFeedback,
  Alert,
  RefreshControl
} from 'react-native';
import firebase from 'firebase';
import Fire from './../../api/StoreApi';
import 'intl';
import 'intl/locale-data/jsonp/en';
import { Icon } from 'react-native-elements'

const db = firebase.firestore().collection('appointment');

export default class HomeScreen extends React.Component {

  static navigationOptions = (navigation) => ({
    header: null
  });

  // State variables
  state = {
    dateItems: [],
    loading: false,
    selectedIndex: 0,
    refreshing: false,
  }

  unsubscribe = null;

  // Get user appointments from the database and filter based on user ID
  async getAppointment() {
    var snapshot = await db.orderBy('createdAt', 'desc')
      .get()

    // Optional argument for the date/time format
    const options = {
      day: 'numeric', month: 'long', year: 'numeric',
      hour: 'numeric', minute: 'numeric',
      hour12: false,
    };

    // Get current user's ID
    const currentUser = firebase.auth().currentUser.uid;

    // Format date
    const dateFormat = new Intl.DateTimeFormat('en-GB', options);

    const dateList = snapshot.docs.filter((doc) => (doc.data().uid === currentUser))
      .map(doc => {
        const dateObj = new Date(doc.data().date.seconds * 1000);
        const createdAtObj = new Date(doc.data().createdAt.seconds * 1000);
        //Return values
        return {
          key: doc.id,
          date: dateFormat.format(dateObj),
          createdAt: dateFormat.format(createdAtObj),
        }
      });
    // Change initial state
    this.setState({
      dateItems: dateList,
      loading: false,
    })
  }

  // Return new list after deleting appointment
  onDelete = () => {
    const newDateList = [...this.state.dateItems];
    newDateList.splice(this.state.selectedIndex, 1);
    // Change state
    this.setState({
      dateItems: newDateList,
      loading: false,
    })
    console.log(newDateList);
  }

  // Delete appointment
  deleteDate(item) {
    db.doc(item.key).delete()
      .catch((error) => console.warn(error));

    this.onDelete();
  }

  // Swipe down to refresh list
  onRefresh = () => {
    this.setState({ refreshing: true });
    setTimeout(() => {
      this.setState({ refreshing: false });
    }, 2000);

  }

  componentDidMount() {
    this.getAppointment();
  }

  render() {
    return (
      <View style={styles.container}>
        // Header
        <View>
          <View style={styles.header}>
            <Text style={styles.headerTitle}>Appointments</Text>
          </View>

          // Show appointments in a list
          <FlatList
            style={styles.list}
            data={this.state.dateItems}
            extraData={this.state.dateItems}
            renderItem={({ item, index }) => (
              <TouchableWithoutFeedback>
                <View style={styles.appointment}>
                  <View style={styles.date}>
                    <Text style={styles.item}>{item.date}</Text>
                    <Text>Created: {item.createdAt}</Text>
                  </View>

                  // Delete icon
                  <View style={styles.icon}>
                    <Icon
                      size={10}
                      reverse
                      name='trash'
                      name='ios-trash'
                      type='ionicon'
                      color='#CA300E'
                      onPress={() =>
                        Alert.alert(
                          'Delete?',
                          'Cannot be undone',
                          [
                            { text: 'Cancle' },
                            { text: 'OK', onPress: () => { this.deleteDate(item) } }
                          ],
                          { cancelable: false },
                        )
                      }
                    />
                  </View>

                  <View style={styles.hr}></View>

                </View>
              </TouchableWithoutFeedback>
            )}
            keyExtractor={(item, index) => {
              return index.toString();
            }}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
              />
            }
            contentContainerStyle={{ flexGrow: 1 }}

          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff'
  },
  header: {
    paddingTop: 50,
    paddingBottom: 16,
    backgroundColor: '#17202a',
    borderBottomWidth: 1,
    borderBottomColor: '#ebecf4',
    shadowColor: '#454d65',
    shadowOffset: { height: 5 },
    shadowRadius: 15,
    shadowOpacity: 0.2,
    zIndex: 10
  },
  headerTitle: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: '500',
    color: '#fff',
  },
  appointment: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 1,
  },
  item: {
    fontSize: 18,
    fontWeight: 'bold',
    margin: 10,
  },
  hr: {
    borderBottomColor: '#d1d0d4',
    borderBottomWidth: 1,
  },
  list: {
    margin: 10,
  },
  icon: {
    alignItems: 'flex-end',
    flex: 1,
  },
  btns: {
    alignItems: 'center',
    margin: 0,
    padding: 2
  },
  add: {
    paddingLeft: 10,
    alignItems: 'flex-start',
  },
  logout: {
    paddingRight: 10,
    alignItems: 'flex-end',
  }
});
