import React, { useState } from "react";
import { View, StyleSheet, Platform } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import firebase from 'firebase';
import { Button } from 'react-native-elements'

const Booking = () => {
    // State variables
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);

    // Change show and mode states
    const showMode = currentMode => {
        setShow(true);
        setMode(currentMode);
    };

    // Show date picker, when button is pressed
    const showDatepicker = () => {
        showMode('date');
    };

    // Show time picker, when button is pressed
    const showTimepicker = () => {
        showMode('time');
    };

    // Assign new date and time once selected
    const onChange = (event, date) => {
        const currentDate = date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);

    };

    // Send selected date and time to the database
    const addAppointment = () => {
        firebase.firestore()
            .collection("appointment")
            .add({
                uid: firebase.auth().currentUser.uid,
                date: firebase.firestore.Timestamp.fromDate(date),
                createdAt: firebase.firestore.FieldValue.serverTimestamp()
            }).then((snapshot) => snapshot.get())
            .catch((error) => alert(error));
    }

    return (
        <View>
            <View>

                // Date button
                <View style={styles.picker}>
                    <Button
                        onPress={showDatepicker}
                        title="Show date picker!"
                        type="outline"
                        raised={true}
                    />
                </View>

                // Time button
                <View style={styles.picker}>
                    <Button
                        onPress={showTimepicker}
                        title="Show time picker!"
                        type="outline"
                        raised={true}
                    />
                </View>
                {show && (
                    <DateTimePicker
                        testID="dateTimePicker"
                        timeZoneOffsetInMinutes={0}
                        value={date}
                        mode={mode}
                        is24Hour={true}
                        display="default"
                        onChange={onChange}
                    />
                )}
            </View>

            <View style={styles.btn} >
                <Button
                    onPress={() => addAppointment(onChange)}
                    title="Book"
                    type="outline"
                    raised={true}
                />
            </View>
        </View >

    );
};

export default Booking;

const styles = StyleSheet.create({
    header: {
        marginTop: 0,
        padding: 10,
        width: 1000,
        backgroundColor: '#e6ebf0',
    },
    h1: {
        color: '#17202a',
        fontSize: 25,
    },
    picker: {
        margin: 10,
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btn: {
        marginTop: 30,

        padding: 30,
        borderRadius: 100,
    },
    dateText: {
        color: 'crimson',
        fontSize: 20,
        marginBottom: 20,
    }
});
